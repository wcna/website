---
title: March 2020 Newsletter
tags: 
    - newsletter
excerpt: TWillow Creek Neighborhood Association newsletter for March 2020.
createdAt: 2020-03-12 11:26:00
---

**February was a great month.** THE GATES between Rock Springs and Willow Creek were finally opened, giving us a second way to enter and drive out of Willow Creek.  Your Willow Creek Neighborhood Temporary board members, President Deanna and Vice President Leesa worked very hard to get those gates opened.  It was a Safety problem for all homeowners and emergency vehicles experienced when the front of Willow Creek was shut down, because of accidents.  Remember, the speed limit for Rock Springs is 20mph.

**The Willow Creek Italian Cookoff has be postponed until a future date.  The new date and time will be noted on a News Letter.** 

Willow Creek is still planning an **EASTER EGG HUNT** in April for our children.  Since it will be outside and not inside a room, we fill it will be good to continue the plans.  It will be in one of the common areas.  The Egg Hunt will be Saturday, April 4, from 11:00 a.m. until 12:30 p.m. Signs on Baywood and at the entrance will show you the way.  The common area is one of the largest in Willow Creek.  Your child will need to bring a basket or a bag to put their eggs in.  The common area will be set up for the Easter Egg Hunt and for your children.  Come enjoy the fun and excitement of the children while they hunt for eggs!

The temporary Board is working on a date, time and place for the **ELECTION OF BOARD MEMBERS** in June.  Any homeowners that are interested in running for the Board, need to contact the Temporary Board at [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com).  Remember, that you should have the commitment and time to serve on the board and attend the board meetings.  There will be a meeting before the **ELECTION DAY**, for you to attend and put your name on the ballot. A list of the duties for each position will be in a News Letter before the meeting.  Join the Board and help make this Community great.

**Remember that donations are still needed to help pay off the past due LG&E bill.  If each homeowner paid $50.00, we would have enough to pay the past due LG&E bill in full, make the monthly payments on time, and also pay for improvements to our neighborhood.**

Any questions or suggestions can be emailed to [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com)
The WCNA’s mailing address is PO Box 258, Crestwood, KY 40014.

**SCRAP METAL COLLECTION IN THE NEIGHBORHOOD:** 
If you have metal to dispose of.  Joe Catfish: 502-741-8872