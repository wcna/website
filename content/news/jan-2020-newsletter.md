---
title: January 2020 Newsletter
tags: 
    - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for January 2020.
createdAt: 2020-01-12 11:26:00
---

**THE WILLOW CREEK HOMEOWNERS ASSOCIATION IS NO LONGER IN EXISTENCE**

A new WILLOW CREEK NEIGHBORHOOD ASSOCIATION (WCNA) has been filed with the Kentucky State Treasurer. WCNA is operating as a non-profit with an EIN #. This is required by the IRS for non-profits with or without employees. **The bank checking account and the locks on the PO Box have been changed.** To get this done, volunteers setup a temporary board until a new election to be held in June. The election date will be announced via the Newsletter, email, Nextdoor, and Facebook. Please consider what position you would like before the new election. Homeowners who want to be on the board need to have the time and ability to commit to board member duties: Attend meetings, return phone calls, plan and monitor projects in a timely manner. If you do not have the time to be on the board, there are many other ways to help your neighborhood as a volunteer. **Neighborhood Association is different from a Homeowner Association in that participation and the dues are voluntary.** There will be reminders in the newsletters, emails, Nextdoor and Facebook. A donation of at least $50.00 per year would keep the street lights on and pay for grass cutting of the common areas not cut by volunteers. The average monthly LGE bill is around $710.00 per month, totaling about $9000.00 per year. The yearly grass cutting cost would depend on how much is cut by volunteers. $50.00 per year is only $4.17 per month or .14 cents per day. This is not much to keep our street lights on for the children going to the bus stops, people walking in the dark, and keeping our neighborhood safe.

A Neighborhood Association’s goal is to build a community that will benefit everyone. Keeping the community aware of all financial needs and financial statements. Organize community work days to keep our neighborhood looking fit and trim. Welcoming new neighbors, helping them feel part of our neighborhood.

---

**Remember, money is needed to pay the LGE electric bills to keep our street lights on. Right now, Willow Creek is about $5,620.00 past due on our electric bill. It is our #1 priority to get this paid. We need as many donations, as you can give, to help pay this bill. If you can think of a fundraiser to raise the money needed, it would be extremely appreciated.** Most of the common areas have been cut voluntarily by homeowners. We would appreciate anyone willing to cut the common areas close to their home. If we could get volunteers for the cuttings, it would not be necessary to pay anyone to cut any area.

**The mailing address for WCNA is: PO Box 258, Crestwood, KY 40014.**

**The new email address for WCNA is: [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com)**

---

If you would like for your WILLOW CREEK NEIGHBORHOOD NEWS and Financial Statement sent via email, **please** send your email address with your name and address to [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com). Some of the email addresses we have received at past meetings were illegible. The newsletter will also be posted on Willow Creek Neighborhood Nextdoor and Willow Creek Facebook, **to help save on cost and time for printing and delivery of the Newsletter.**

---

**A POT LUCK FOR ALL RESIDENTS OF WILLOW CREEK IS SCHEDULED FOR SATURDAY JANUARY 25, 2020 AT WATKINS CHURCH - 9800 WESTPORT ROAD, 40241. FROM 1pm UNTIL 4pm. THIS ROOM IS BEING SUPPLIED FOR FREE BY THE CHURCH. PLEASE COME AND GET TO KNOW YOUR NEIGHBORS. BRING A DISH AND YOUR DRINKS. (VEGETABLES, SALADS, DESSERTS, ETC). WE WILL TRY TO KEEP TABS ON WHICH SIDES WE MAY BE IN NEED OF THROUGH FACEBOOK. WE WOULD LIKE TO HAVE A CHILI CONTEST DURING THE POT LUCK. BRING YOUR BEST CHILI SO WE CAN ENJOY. MAYBE YOU WILL WIN 1 ST PLACE AND HAVE THE NEIGHBORHOOD BRAGGING RIGHTS! THERE WILL BE A QUESTION AND ANSWER SESSION WITH THE FOLLOWING GUESTS: CENTER FOR NEIGHBORHOODS (LOUISVILLE) TAI COATLEY AND MICHAEL FORBUSH – OLDHAM COUNTY MAGISTRATE, BOB DYE – AND POSSIBLY OUR COUNCILMAN FROM JEFFERSON COUNTY, SCOTT REID. NO CHILD CARE PROVIDED. NO ALCOHOL OR PETS PLEASE.**

---

**YOU WILL FIND OUR FIRST FINANCIAL STATEMENT WITH THIS NEWSLETTER. WE HAVE REACHED OUT TO THE PREVIOUS TREASURER TO OBTAIN RECORDS PRIOR TO DECEMBER 10th 2019 AND HAVE NOT HEARD BACK. TWO YEARS OF OLD BANK STATEMENTS WERE OBTAINED BY THE NEW TEMPORARY TREASURERS, ONLINE THROUGH THE BANK, NOT FROM THE PAST TREASURER. THE LGE BILL IS NOW UNDER WCNA, AND WE HAVE ACCESS TO THE PREVIOUS BILLS.**

---

The last couple of Willow Creek meetings, several homeowners who attended were mainly concerned about transparency. Several homeowners said they have not paid the $50.00 per year, but would be willing, if they knew where the money was spent. More homeowners than ever were wanting to be involved in the decisions our community needed to make. They wanted to be involved with helping this neighborhood be the best it can be. Hopefully, with this new Neighborhood Association, all of our concerns can be addressed.

**Remember, Neighborhood Associations can not solve neighbor disputes or force homeowners to comply with laws. The police or local government should be called for those types of problems. Neighborhood Association does not have the ability to place liens on properties. But we as neighbors can help other neighbors in need. As a community, we can watch our neighbors’ homes while they are on vacation. We can volunteer to help keep our neighborhood clean on scheduled Neighborhood Cleanup days. Work on getting the Rock Springs gates removed and improving sidewalks.**

**THIS IS YOUR NEIGHBORHOOD. You live here. Your children play here. You sit outside and enjoy the scenery. You take your walks here. This neighborhood can be anything we choose. So, let’s make Willow Creek the best neighborhood we can.**

---

**If you have any information you would like to be included in the NEWSLETTER, send your info to: [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com).**
