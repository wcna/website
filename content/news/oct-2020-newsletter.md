﻿---
title: October 2020 Newsletter
tags: 
    - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for October 2020.
createdAt: 2020-10-12 11:26:00
---

## WILLOW CREEK NEIGHBORHOOD NEWS

OCTOBER is here. HALLOWEEN IS COMING UP! Most homeowners on Willow Creek Facebook and Nextdoor are planning to give out candy. Lots of suggestions on how they are giving out the candy, putting the candy in baggies, taping candy bags to popsicle sticks all over the yard, leaving bowls of candy in the front, and like in the past, handing out candy with a trick or treat. HALLOWEEN CAN HAPPEN. Everyone can be safe and still have a great time. Like always, parents should check the candy before letting the little ones have a bite. This was a regular ceremony at our house with (Dad or Grandpa) (Mom or Grandma) checking the candy at the dining room table, with the children watching very closely.

We want to take this opportunity to give a BIG SHOUT OUT to the neighbors who volunteered to mow the front entrance and the common areas this season, thank you for your hard work and time in keeping our neighborhood looking nice at no cost, it is very much appreciated.

Because of the current COVID‐19. It has been decided that the COMMUNITY PICNIC will not happen this year, this is devastating to all of us as we were really looking forward to this. We were hoping to have a day that our neighborhood could gather together for a great time of food and games. This virus has really put a damper on what we had planned for this year. We have been trying to bring our community together as one neighborhood giving each home owner an opportunity to get to know their neighbors.

Your temporary board will be coming around the neighborhood in the next few weekends, weather permitting, with a survey, it will give us a chance to meet our neighbors and to collect some data and a chance for you to meet some of your temporary board, social distancing guidelines will be followed, stay tuned to Facebook and Nextdoor for dates and streets we will be on.

**WE REALLY NEED YOUR COMMITMENT TO YOUR NEIGHBORHOOD:**
Only 100 home owners/landlords out of 341 homes in Willow Creek have paid the $50 donation in 2020, we cannot pay the LG&E bill down at this rate along with the monthly usage of approx. $740.00 being added to this balance each month, that being said, 241 properties have not donated for 2020. If everyone donated we could collect approx. $17,000.00 a year that would be enough to pay the monthly LGE usage of approx. $9,000 for a year along with the past due balance, and once that is caught up we would have funds for other things we would like to see done here in the years following. We are trying to keep the street lights on, keeping our children safe and the neighborhood safe is of utmost importance to us. This will not happen without everyone’s commitment.

The permanent board election has also been postponed. It was planned for last June, but now there is no place for the neighborhood to gather inside to elect our board members. The temporary board is still planning on having the election, we were getting the meeting room at no cost at Watkins United Methodist Church (shout out to Susie & Chuck Dempsey) and they haven’t even opened up for church services much less the meeting room so once that is available we will get something going on the election.

HOPEFULLY, next year will be a new beginning for all of us with getting our jobs back or new jobs, visiting our parents and grandparents, visiting our sick family members or friends at hospitals or nursing homes, taking our children on adventures at parks and having cookouts with our family or friends, our children being able to have a normal life of school, parties, graduation celebrations, day cares, babysitters, and sleepovers. Being able to give a hug, shake hands, high fives to friends, family members and neighbors. Having the election for our permanent board, having community gatherings, collecting enough donations to bring the LGE bill down, keeping the monthly bills paid and making the improvements we all want for our neighborhood.

**NEWSLETTER CLASSIFIEDS:** If you would like to place a small ad in your monthly newsletter it is $10 per month, send ad information to willowcreekna@gmail.com, make checks payable to Willow Creek Neighborhood Association send to P.O. Box 258, Crestwood, KY. 40014. Deadline to make it in next month’s newsletter will be Nov. 10th . Please consider doing this if you have a small business or crafts to sell I see so many asking for recommendations on social media for various tasks.

YOUR NEIGHBORHOOD WEBSITE :
[willowcreek.community](https://willowcreek.community)
Donate by credit or debit card on a **SECURE SITE**. You can click on the amount you want to donate and it will take you to the secure site to make your donation. If you have any questions, you can contact your temporary Board Members thru the willowcreekna@gmail.com. Several homeowners have already taken advantage of this great site. Thank you.

Any questions or suggestions can be emailed to willowcreekna@gmail.com
The WCNA’s mailing address is: PO Box 258, Crestwood, KY 40014
The BLOCKWATCH email is: willowcreekd16blockwatch@gmail.com

**Please join us on our Willow Creek Facebook Page or Willow Creek Nextdoor**

### CLASSIFIED SECTION
*Proceeds go toward the LGE bill*

**SAUNDERS SMALL ENGINE**
110 CANNONS LANE, LOUISVILLE, KY 40206
502‐895‐1536
OPEN:
MON.THRU FRI. 8:00am TIL 5:00pm
SAT. 8:00am TIL 2:00pm
WE REPAIR AND SALE PARTS FOR: MOWERS, RIDERS, TRACTORS, Z TURNS, CHAINSAWS, TRIMMERS, BLOWERS, CERTAIN GENERATORS & CERTAIN PRESSURE WASHERS
