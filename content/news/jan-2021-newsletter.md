﻿---
title: January 2021 Newsletter
tags: 
 - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for January 2021.
createdAt: 2021-01-12 11:26:00
---

# WILLOW CREEK NEIGHBORHOOD NEWS

HAPPY NEW YEAR! Here’s a toast to everyone who made it through 2020. Hopefully your 2021 days will be better and a new beginning. Thinking about starting a diet or getting in better shape in the new year? If so walking your neighborhood, exploring your community, getting ideas for sprucing up the front yard, greenery for the yard, shutter or door colors is a great way to become familiar with your neighborhood while burning those pesky calories. You should get your lawn equipment ready for cutting season this time of year as well. Did you know you lose most of your heat through the cracks in the exterior doors and windows, this can be prevented by putting weather stripping around those doors and windows. Don’t put film on the inside of the windows if you have a cat or dog that likes to look out the windows, I learned this is from experience! Don’t forget to rake any piles of leaves on your grass. Leaving deep piles of leaves on the grass makes it difficult for your grass to breathe during the cold months, this will leave spots in your yard in the spring.

**ATTENTION RESIDENTS:** Please remit your $50 for street lights by 01.31.21 to cover 2021. It is so important we get everyone on a set payment schedule in order to budget for the year. If you have any questions, please feel free to send us an email. If you can’t get your payment in by 1.31.21 please send it when you can, we will accept what you can afford to send and when you can afford to send it, all payments will be appreciated and accepted after 01.31.21. **Please make sure you read the letter to residents included in this newsletter.**

We have had several residents reporting of a person or persons checking out cars in our community. So, keep a look out, stay safe and report any sightings to the police. If you have outside camera videos check them out and send any suspicious activity to the police. Please don’t keep any items in your cars that someone would like to steal. Do not leave a gun in your car even if the doors are locked and it is out of sight. Some of the videos shared by neighbors the person opened the car doors and looked inside to see if there was anything worth grabbing. STAY SAFE AND IF YOU SEE SOMETHING SAY SOMETHING!! LMPD and OC Police will take a report, they track this activity and chances are they know who the perpetrators are.

Your temporary board is hoping that we can plan gatherings for our community this spring, summer and fall, so that you can meet your neighbors and get to know them. We live in a great community, and need to get together in happier times. On the proposed agenda for 2021 is organizing an election for your permanent board, community picnic, pot luck dinner, decorating for the holidays contests, and another community yard sale.

We are working on transitioning the newsletter to email month instead of delivering door to door. If we haven’t already collected your email address please provide it to us at [willowcreekna@gmail.com](willowcreekna@gmail.com) We will continue to deliver the newsletter to those without email or who prefer paper. As of the printing of this letter we have collected 119 emails out of 340 homes, please send us your email this will cut down on paper, ink, plastic bags, and delivery time for your board members.

Remember that $50 a year is needed from all residents to keep the street lights on, that’s .14¢ a day for the safety of our children and our neighborhood. Please send your $50 payment to: WCNA – PO Box 258, Crestwood, KY 40014 or visit the website: [willowcreek.community](https://willowcreek.community) and pay with your debit or credit card via our secured site.

**NEWSLETTER CLASSIFIEDS:** If you would like to place a small ad in your monthly newsletter it is $10 per month, send ad information to [willowcreekna@gmail.com](willowcreekna@gmail.com), make checks payable to Willow Creek Neighborhood Association send to P.O. Box 258, Crestwood, KY. 40014. Deadline to make it in next month’s newsletter will be the Feb. 10th.
Any questions or suggestions can be emailed to: [willowcreekna@gmail.com](willowcreekna@gmail.com) 
WCNA mailing address: PO Box 258, Crestwood, KY 40014
Website: [willowcreek.community](https://willowcreek.community), donate securely here
BLOCKWATCH: [willowcreekd16blockwatch@gmail.com](willowcreekd16blockwatch@gmail.com) 
Please join us on the Willow Creek Neighborhood Facebook Page.

## CLASSIFIED SECTION
*Proceeds go toward the LGE bill ‐ $10.00 a month*

**SAUNDERS SMALL ENGINE**
110 CANNONS LANE
LOUISVILLE, KY 40206 
502‐895‐1536
OPEN: MON.THRU FRI. 8:00am TIL 5:00pm; SAT. 8:00am TIL 2:00pm
WE REPAIR AND SELL PARTS FOR: MOWERS, RIDERS, TRACTORS, Z TURNS, CHAINSAWS, TRIMMERS, BLOWERS, CERTAIN GENERATORS & CERTAIN PRESSURE WASHERS


***
## LETER TO RESIDENTS

Hello Residents,

The board is asking everyone to pay their share of the LG&E by 01.31.21 in order to get as many residents as possible on a set payment schedule, this will allow us to know how much money we have for the year to cover the monthly LG&E charges and to make plans for fund raisers to cover any short fall we may have. Of course we realize that COVID has been a hardship for many of us, however $50.00 a year breaks down to 0.14¢ per day, if you can’t pay $50 please pay what you can when you can even if it’s after 01.31.21 funds will always be accepted and appreciated.

Willow Creek is outside of the Louisville Metro city limits therefore we are responsible for our street In October 2020 LG&E forced us into a payment plan to keep the lights on beginning with the December 2020 payment adding $268.62 a month to our monthly charge of approx. $730.00 a month in order to keep the street lights on. LG&E was told at that time we would not be able to make those payments every month, we were able to pay $980.95 in December 2020, however come January there was not enough money in the bank account to pay the requested $999.46, we did pay the monthly charge of $730.84, by going on the payment plan it did buy us some time before they cut us off. The President of our board is in constant communication with LG&E and the Jefferson County district councilman’s office trying to work on a solution for the past due amount.lights as a neighborhood, we need all residents to pay their share for the lighting and are requesting $50.00 a year from each of you to be paid by 01.31.21. If you paid in November or December 2020 we will count that toward 2021 and you will be good until January 2022.

The majority of neighborhoods in this area have HOA’s with yearly dues in the hundreds of dollars or they are 6th class cities where the residents pay taxes to their city or they just don’t have street lights. We are not an HOA but a Neighborhood Association, there is a difference. Oldham County Police and LMPD have urged us to do what we can to keep the lights on and they have expressed how important it is in curbing crime and keeping residents safe and each home should be well lit at night, so turn those porch lights on folks.

Let this email serve as an invoice for your record of payment of $50.00. Payments can be made by check or securely via our website at the following:
Willow Creek Neighborhood Association
P.O. Box 258
Crestwood, KY. 40014
[willowcreek.community](https://willowcreek.community)

As always your board appreciates you and your help with keeping the lights on and putting this past due balance in our rear view, we can’t do it without you. If you have any questions or concerns please contact us at [willowcreekna@gmail.com](willowcreekna@gmail.com).

Sincerely,
Carla Harbin
Treasurer
