﻿---
title: September 2020 Newsletter
tags: 
    - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for September 2020.
createdAt: 2020-09-12 11:26:00
---

## WILLOW CREEK NEIGHBORHOOD NEWS

SEPTEMBER has arrived! We have already celebrated LABOR DAY, PATRIOT DAY, GRANDPARENTS DAY, and ROSH HASHANAH. We still get to celebrate the FIRST DAY OF AUTUMN and YOM KIPPUR. Most schools have opened virtually or in person. The end of September or first of October is a good time to sow grass seed. The soil is still warm and the cool evenings make for fast germination. Try to sow the grass seeds before it rains. If you like Tulips or Daffodils, early October is a good time to plant the bulbs. Be sure to read the directions, so you don’t plant them too deep or too close to the top. Late September is a also a good time to plant most trees and shrubs.

Keep reading the NEWSLETTER to find out if and when our COMMUNITY PICNIC will happen this year. When you see a survey on Facebook or Next Door, please vote, so we can get a good idea of how many will participate. If you can’t get on Facebook or Next Door, drop a email to willowcreekna@gmail.com. The details will be the same as your special sheet stated in July. Same place, same details, just different date. The ending time may change depending on when the sun goes down. Keep in mind, all profits will still go toward the large LGE BILL. It would be a wonderful thing, if more home owners donated their share of $50.00 per year. We need every homeowner to participate.

We will be looking for anyone who can give a little time, to help the people who are manning the games or food area for the picnic.

**THE COMMUNITY YARD SALE WAS A GREAT SUCCESS!!!!** We made $560.00 . It was deposited in the bank, to be applied to the LGE street light bill. Your temporary Board wants to thank everyone who helped make this happen. We had a lot of volunteers who helped at the community yard sale, Deanna, Carla, Lisa, Susie, Donna, Chris, Roger, Dennis and Beverly. Thomas and Heather volunteered their time, to be our DJ’s for the entertainment and their son, Trey provided the Lemonade stand. A special thanks goes to Carla, for all of her time and space. And we really want to thank all of the homeowners who donated a lot of great items to be sold at the yard sale. We also thank the other homeowners who had their own yard sale at their homes, this drew a lot of people to our Community. Any items that were not sold, were donated to the Good Will.

**NEWSLETTER CLASSIFIEDS:** If you would like to place a small ad in your monthly newsletter it is $10 per month, send ad information to willowcreekna@gmail.com, make checks payable to Willow Creek Neighborhood Association send to P.O. Box 258, Crestwood, KY. 40014. Deadline to make it in next months newsletter will be the Oct. 10th .

**YOUR NEW WEBSITE:** [willowcreek.community](https://willowcreek.community) **CHECK IT OUT.....** It is up and running to donate by credit or debit card on a **SECURE SITE**. You can click on the amount you want to donate and it will take you to the secure site to make your donation. If you have any questions, you can contact your temporary Board Members thru the willowcreekna@gmail.com. Several homeowners have already taken advantage of this great site. Thank you.

Any questions or suggestions can be emailed to willowcreekna@gmail.com
The WCNA’s mailing address is: PO Box 258, Crestwood, KY 40014
The BLOCKWATCH email is: willowcreekd16blockwatch@gmail.com

**Please join us on our Willow Creek Facebook Page or Willow Creek Nextdoor**

### CLASSIFIED SECTION
*Proceeds go toward the LGE bill*

**SAUNDERS SMALL ENGINE**
110 CANNONS LANE, LOUISVILLE, KY 40206
502‐895‐1536
OPEN:
MON.THRU FRI. 8:00am TIL 5:00pm
SAT. 8:00am TIL 2:00pm
WE REPAIR AND SALE PARTS FOR: MOWERS, RIDERS, TRACTORS, Z TURNS, CHAINSAWS, TRIMMERS, BLOWERS, CERTAIN GENERATORS & CERTAIN PRESSURE WASHERS
