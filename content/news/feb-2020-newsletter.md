---
title: February 2020 Newsletter
tags: 
    - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for February 2020.
createdAt: 2020-02-12 11:26:00
---

**January was a great beginning.**  With a temporary board in place, control of the money now belongs to the homeowners in Willow Creek.  The past due LG&E bill is slowly being paid as soon as the money is accumulated in the bank account.  Thanks to all that have contributed!

The **Chili Cook-Off** was a success. Eight different chilis were submitted as well as lots of other great food. We had hot dogs (to go with the chili), Bar-B-Q meat balls, mac and cheese, pork and beans, potato salad, pasta salad, fresh fruit, cornbread, grilled cheese sandwiches, a ton of desserts, lemonade, etc. Four speakers from our government and Center for Neighborhoods attended to answer questions. The chili contest change war raised $94.43, and additional donations brought the total to $215.43 towards the overdue electric bill. The WINNER of the Chili Cook-Off was Chris Hornback! He has the bragging rights. **Congratulations!** It was wonderful to meet and get to know our neighbors. Thanks to Watkins Church for furnishing the large room. They were there to help us set up the room and clean up.

Willow Creek is now planning an **ITALIAN COOK-OFF**. The cook off will be at Watkins Church, 9800 Westport Road, 40241, on Saturday, March 28, from 3:00 to 5:00 p.m. Bring your favorite Italian dish to share with your neighbors (and bring change to vote for your favorite dish! All money raised goes to the overdue electric bill). Plates, bowls, utensils, napkins and cups will be furnished. Bring your own drink. No speakers as of now. Plan now to come and meet your neighbors and have a great meal. Hope to see you and your family there!

Willow Creek is also planning an **EASTER EGG HUNT** in April for our children. It will be in one of the common areas. It is planned for Saturday, April 4, from 11:00 a.m. until 12:30 p.m. Signs on Baywood and at the entrance will show you the way. The common area is one of the largest in Willow Creek. Your child will need to bring a basket or a bag to put their eggs in. The common area will be set up for the Easter Egg Hunt and for your children. Come enjoy the fun and excitement of the children while they hunt for eggs!

**URGENT MESSAGE TO ALL RESIDENTS OF WILLOW CREEK.** On Sunday night 2/02/2020, a white male armed with a knife attempted to break in a home on Kendrick Drive while the homeowners were home. He was apprehended and taken to jail. According to the police, he was wanted for various other offenses. PLEASE KEEP AN EYE OUT FOR STRANGERS WALKING IN OUR NEIGHBORHOOD. KEEP YOUR HOMES AND CARS SECURE AT ALL TIMES. WATCH THE CHILDREN WHILE THEY ARE PLAYING OUTSIDE.

**UPDATE:** Male that was breaking into the vehicles in Willow Creek, during the evening last week was arrested by Police. Due to the RING DOOR videos posted on Facebook, Willow Creek and Next Door, Willow Creek. Thanks to all that posted the videos. They were turned over to the Police which assisted with the arrest.

**YOUR TEMPORARY WILLOW CREEK NEIGHBORHOOD ASSOCIATION BOARD MEMBERS**
*President:* Deanna Hornback
*Vice President:* Leesa Card
*Treasurers:* Susie Dempsey, Carla Harbin, Rosemary Ross
*Secretaries:* Lisa Cochran, Donna Richardson
*Trustees:* Mickayla Bell, Lauren Fernandez, Beverly Lewis
*Sergeant-At-Arms*: Chris Hornback

Remember that donations are still needed to help pay off the past due LG&E bill. If each homeowner paid $50.00, we would have enough to pay the past due LG&E bill in full, make the monthly payments *on time*, and also pay for improvements to our neighborhood.

Any questions or suggestions can be emailed to [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com)
The WCNA’s mailing address is PO Box 258, Crestwood, KY 40014.

**PHONE NUMBERS YOU MIGHT WANT TO KEEP HANDY:**

|Agency|Jefferson County|Oldham County|
|--|--|--|
|Emergency (Police, Fire, EMS)|911|911|
|Animal Control|473-7387|222-7387|
|Code Enforcement|574-2508|222-1476|
|County Attorney|574-6336|222-7342|
|County Clerk|574-5700|222-9311|
|Driver’s License|595-4405|222-1979|
|Health Department|574-6520|222-3516|
|Motor Vehicle Registration|574-5700|222-7645|
|Police Department|574-7060|222-1300|
|Red Cross|589-4450|222-0308|
|Road Department|574-5810|222-0426|
|Sheriff’s Department|574-5400|222-9501|
|Voter Registration|574-6100|222-0047|

Jefferson County Metro Councilman (16th District): Scott Reid, 574-1116
Oldham County Magistrate (7th District): Bob Dye, 423-7371

#

### President's Letter

Dear Neighbor,

As you may already be aware, the HOA was dissolved in October of 2018. A group of homeowners began meeting in October 2019 to discuss needed changes. Over the course of three neighborhood meetings, we developed a list of volunteers that showed interest in helping improve the neighborhood, and a temporary board was formed.

December 10th we filed with the Secretary Of State as a Neighborhood Association which is a voluntary, resident-driven organization that focuses on community rather than regulations. Homeowners and renters alike are welcome to join the Neighborhood Association. We request $50.00 a year (only 14 cents a day) to keep the lights on and help us clean up the neighborhood.

YOU WILL KNOW HOW YOUR MONEY IS SPENT! We started a monthly newsletter that includes a financial statement. If you have an email address, please contact [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com) to help us keep printing costs down.

Many neighbors have expressed wanting to pay for past years. We welcome any donations that you can make as we still have an outstanding LG&E bill of over $4,000 dollars. The money just was not coming in to cover the bill. The current bill for the street lights is $732.00 a month.

We are brainstorming ways to bring everyone together to help improve our neighborhood! Our first potluck on January 25th was a great time with about 40 people in attendance. We will have many more events in hopes to include a lot more neighbors.

An official board election will be held in June. If you would like to run for a board position, please consider which position and if you can commit to the time investment. Those unable to commit to a board position will have lots of other opportunities to help. Please email any questions or suggestions to [willowcreekna@gmail.com](mailto:willowcreekna@gmail.com).

This is our neighborhood. This is where we live and raise our families. Let’s look out for one another’s safety and well-being and make Willow Creek a place to be proud of!

Sincerely,
Deanna Hornback
Willow Creek Neighborhood Association President

#

### WILLOW CREEK NEIGHBORHOOD ASSOCIATION

*March 1, 2020 - March 1, 2021*

Become A Member Of your Neighborhood Association for only $50.00 A year! This breaks down to .14 cents a day! Please help support your neighborhood needs!!!

Membership Includes

 - Street Lights will stay on
 - Neighborhood events/get-togethers
 - Neighborhood Meetings
 - Neighborhood Newsletter
 - Beautification of the neighborhood
 - Voting power
 - This membership will help improve our neighborhood

Please send payment to:

WCNA
PO Box 258
Crestwood KY 40014