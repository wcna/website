---
title: August 2020 Newsletter
tags: 
    - newsletter
excerpt: Willow Creek Neighborhood Association newsletter for August 2020.
createdAt: 2020-08-12 11:26:00
---

## WILLOW CREEK NEIGHBORHOOD NEWS

AUGUST is here. We don’t have a special holiday to celebrate in August, but there are Birthdays and Anniversaries’ a lot of families are celebrating. Some schools are opening on different schedules. More people are venturing out of their homes. Some restaurants and businesses are reopening for business, not back to normal, but it’s a beginning.

ON A MORE IMPORTANT NOTE, COVID‐19 is still with us. Maybe not a bad as in March, but we are still under executive orders from our Governor to social distance and continue to wear face masks.

Deanna, our Temporary Board President did a survey on Willow Creek Facebook and Willow Creek Next Door to get an idea how the neighborhood homeowners felt about the WILLOW CREEK COMMUNITY PICNIC. By far, more homeowners did not want to have the picnic at this time as those wanting to go forward with it, we went with the majority vote to postpone. Most home owners wanted to wait until it was cooler, if the mask is required or until a mask was not required, so the Board has decided to postpone the picnic until (hopefully) sometime in OCTOBER. Please keep reading the NEWSLETTER to find out the exact date. The details will be the same as your special sheet inserted with lasts month newsletter, same place, same details, just different date. The time may change due to earlier sundown. Keep in mind, all profits will go toward the large LGE BILL. It would be a wonderful thing, if more home owners paid their fair share of $50.00 per year.

We will still be looking for anyone who can give a little time to help the people who are manning the games or food area, so they can take a break. We now have a UK and a UL corn hole platform. We also have a new game, called LADDER BALL. This will be great for adults and children.

### UPCOMING NEWS

Any one providing a service and wants to advertise in the CLASSIFIED SECTION in the News Letter, you can still email the information to willowcreekna@gmail.com. The cost is $10.00 for four lines a month. All money received from the paid advertisements will go toward the LGE Bill.

THE COMMUNITY YARD SALE is STILL ON for September 12th . Anyone that does not want to have a yard sale can donate items that are in working condition, clean and saleable to be sold at the Willow Creek neighborhood yard sale site on Eastport Dr, contact us at willowcreekna@gmail.com for the location and to schedule a time to drop your items off. Any money received for the donated items will go toward our LGE bill. We will be posting the event on local area social media pages, such as Oldham County Grapevine, Next Door, and Facebook to get maximum exposure for your yard sale.

**YOUR NEW WEBSITE:** [willowcreek.community](https://willowcreek.community) **CHECK IT OUT.....** We’re a little behind on adding the latest news letters and neighborhood info, but it is up and running to donate by credit or debit card on a **SECURE SITE**. You can click on the amount you want to donate and it will take you to the secure site to make your donation. If you have any questions, you can contact your temporary Board Members thru the willowcreekna@gmail.com. Several homeowners have already taken advantage of this great site. Thank you.

Any questions or suggestions can be emailed to willowcreekna@gmail.com
The WCNA’s mailing address is: PO Box 258, Crestwood, KY 40014
The BLOCKWATCH email is: willowcreekd16blockwatch@gmail.com

**Please join us on our Willow Creek Facebook Page or Willow Creek Nextdoor**

### CLASSIFIED SECTION

*Proceeds go toward the LGE bill*

**SAUNDERS SMALL ENGINE**
110 CANNONS LANE, LOUISVILLE, KY 40206
502‐895‐1536
OPEN:
MON.THRU FRI. 8:00am TIL 5:00pm
SAT. 8:00am TIL 2:00pm
WE SERVICE, REPAIR AND SELL PARTS FOR: MOWERS, RIDERS, TRACTORS, Z TURNS, CHAINSAWS, TRIMMERS, BLOWERS, CERTAIN GENERATORS & CERTAIN PRESSURE WASHERS