import DefaultLayout from '~/layouts/DefaultLayout.vue';
import EmptyLayout from '~/layouts/EmptyLayout.vue';

import BootstrapVue from 'bootstrap-vue';
import VueFuse from 'vue-fuse';
import '@stripe/stripe-js';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { config, library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import '@fortawesome/fontawesome-svg-core/styles.css';

import '~/resources/scss/theme.scss';

require('typeface-rubik');

config.autoAddCss = false;
library.add(fas);
library.add(fab);

export default function (Vue, { head }) {
    Vue.component('DefaultLayout', DefaultLayout);
    Vue.component('EmptyLayout', EmptyLayout);
    Vue.component('font-awesome', FontAwesomeIcon);

    Vue.use(BootstrapVue);
    Vue.use(VueFuse);

    head.meta.push({
        charset: 'utf-8'
    });

    head.meta.push({
        key: 'description',
        name: 'description',
        content: 'Official site of the Willow Creek Neighborhood Association of Louisvlle, KY.'
    });

    head.meta.push({
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
    });

    head.meta.push({
        name: 'google-site-verification',
        content: '5DgqmkjMfnTJY-mXrk2xMfHGcDbhPl0w0UV-RW_Q_Wk'
    });

    head.meta.push({
        name: 'google-site-verification',
        content: '5DgqmkjMfnTJY-mXrk2xMfHGcDbhPl0w0UV-RW_Q_Wk'
    });

    head.script.push({
        src: 'https://js.stripe.com/v3/',
    });
}